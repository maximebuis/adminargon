import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from ".././service/api.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  focus;
  focus1;
  users;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this.apiService.getUsers().subscribe((data: any) => {
      console.log(data);
      this.users = data.data;
    });
  }

  login() {
    this.router.navigate(["home"]);
  }
}

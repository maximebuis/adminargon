import { Injectable } from "@angular/core";
//import { User } from "../model/user.model";
import { Observable } from "rxjs/index";
import { catchError, tap } from "rxjs/operators";
import { throwError } from "rxjs";
import { retry } from "rxjs/operators";
import { Users } from "../users";
import {
  HttpHandler,
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
@Injectable()
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      //  'Authorization': 'Bearer ' +'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzEiLCJlbWFpbCI6ImRlbW9AZ21haWwuY29tIn0.CGVzsWyK3f1LOnsDQKUlQBOa2mp5AO1_s_s9ZnLstps',
    }),
  };
  URLbase = "https://api.solutions-mobiles.com/";

  // Http Options

  getUsers() {
    return this.httpClient
      .get(
        `https://api.solutions-mobiles.com/api/posts?page=1&per_page=50&order_id=id&order_by=asc`
      )
      .pipe(
        catchError((error) => {
          return throwError("Something went wrong!");
        })
      );
  }
}

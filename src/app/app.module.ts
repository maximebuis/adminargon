import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { AppRoutingModule } from "./app.routing";
import { ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { SignupComponent } from "./signup/signup.component";
import { LandingComponent } from "./landing/landing.component";
import { ProfileComponent } from "./profile/profile.component";
import { HomeComponent } from "./home/home.component";
import { SidebarComponent } from "./shared/sidebar/sidebar.component";
import { NavbarComponent } from "./shared/navbar/navbar.component";
import { FooterComponent } from "./shared/footer/footer.component";
import { HomeModule } from "./home/home.module";
import { LoginComponent } from "./login/login.component";
import { ApiService } from "./service/api.service";
import { HttpClient } from "@angular/common/http";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorIntercept } from "./error.interceptor";
import { ListUserComponent } from "./user/list-user/list-user.component";
import { AddUserComponent } from "./user/add-user/add-user.component";
import { EditUserComponent } from "./user/edit-user/edit-user.component";
import { ListPostComponent } from "./post/list-post/list-post.component";
import { AddPostComponent } from "./post/add-post/add-post.component";
import { EditPostComponent } from "./post/edit-post/edit-post.component";
import { ListAdminComponent } from "./admin/list-admin/list-admin.component";
import { AddAdminComponent } from "./admin/add-admin/add-admin.component";
import { EditAdminComponent } from "./admin/edit-admin/edit-admin.component";

import { MatSliderModule } from "@angular/material/slider";
@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LandingComponent,
    ProfileComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    ListUserComponent,
    AddUserComponent,
    EditUserComponent,
    ListPostComponent,
    AddPostComponent,
    EditPostComponent,
    ListAdminComponent,
    AddAdminComponent,
    EditAdminComponent,
  ],
  imports: [
    MatSliderModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HomeModule,
  ],
  providers: [ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
